FROM centos:7.4.1708

RUN yum -y update && yum clean all

ENV CLAM_VERSION=0.99.2

RUN yum install -y epel-release && \
    yum -y install sha3sum openssl-devel epel-release && \
    yum remove -y epel-release

RUN yum install -y make wget gcc openssl && \
    wget https://www.clamav.net/downloads/production/clamav-${CLAM_VERSION}.tar.gz && \
    tar xvzf clamav-${CLAM_VERSION}.tar.gz && \
    cd clamav-${CLAM_VERSION} && \
    ./configure && \
    make && make install && \
    mkdir /usr/local/share/clamav && mkdir /var/lib/clamav && \
    wget -O /var/lib/clamav/main.cvd http://database.clamav.net/main.cvd && \
    wget -O /var/lib/clamav/daily.cvd http://database.clamav.net/daily.cvd && \
    wget -O /var/lib/clamav/bytecode.cvd http://database.clamav.net/bytecode.cvd && \
    yum remove -y gcc make wget && \
    rm -rf /clamav-${CLAM_VERSION}*

RUN yum install -y git && \
    git clone https://github.com/sstephenson/bats.git && \
    cd bats && \
    ./install.sh /usr/local && \
    yum remove -y git

RUN yum install -y sshpass openssh-clients openssl openssl-devel
# In removing gcc it also removes openssl so I have to reinstall it. Fix?
RUN yum update -y && yum clean all
RUN mkdir /var/run/clamav && \
    chmod 750 /var/run/clamav

# Configure Clam AV...
COPY conf/*.conf /usr/local/etc/
COPY tests /tests
COPY import.sh /import.sh
RUN mkdir -p /import && mkdir -p /tests/workspace && mkdir -p /root/tmp && mkdir /root/keys && mkdir /root/.ssh

#ENTRYPOINT ["bash"]

ENTRYPOINT ["bash","/import.sh"]
