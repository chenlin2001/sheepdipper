# SheepDipper - For Uploading Files into the Secure Environment.

## Prerequisites.

### Docker install for Windows.

Click this link to download the Docker toolbox for Windows.
https://download.docker.com/win/stable/DockerToolbox.exe

First things first open the Docker Quickstart terminal and watch as it creates the VM where docker will run, at the end of this it will output some enironment variables which you'll need to export.

They'll look something like this:
```
DOCKER_HOST=tcp://192.168.99.101:2376
DOCKER_CERT_PATH=/Users/<USER>/.docker/machines/.client
DOCKER_TLS_VERIFY=1
DOCKER_MACHINE_NAME=<VMNAME>
```

Alternatively run `docker-machine env <VMNAME>`

Copy these variables to you .bashrc to make then persistant.

Once there you have installed the Prerequisites.

before you run any commands run `boot2docker ssh` to interact within the linux ecosystem rather than windows.

### Docker install for linux

Follow this guide here: https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/

### Docker install for mac

Follow this guide here: https://docs.docker.com/docker-for-mac/install/#install-and-run-docker-for-mac

## Overview

```
NAME:
SheepDipper - A script to automate the process needed to move files from the internet to the secure environment

USAGE:
./sheepdipper.sh [global options] command

GLOBAL OPTIONS
-d                   turn on debug
-h | --help          print the help for this script

COMMANDS
--compile            Takes folder of artifacts and processes them and outputs a file ready to be sent to DevOps.
--upload             Takes processed folder uploads to jump server following all security guidelines.
--filepath | -f      Required option, points to the folder which hold the files you would either like to import of compile.
```

## Compiling Files

If you are looking to compile files to send to the person uploading, then you need to follow these instructions.

1. If this file is dependancies/applications run a vulnerability scan first, and ensure that it is kept separate from the artifacts you'd like to import.

2. Run `docker run -ti --rm -v <Path to artifacts>:/import registry.gitlab.com/gileshinchcliff/sheepdipper:latest -f <PATH TO ARTIFACTS> --compile`

3. Note down the password that's output into the terminal, and upload your files onto box.

4. Pass the password over a different secure means and BOOM!! Your files are ready to be uploaded.

## Uploading Files To Secure Environment

### Prerequisites

If you are uploading files to the secure environment then you also need to configure your ssh config so the it doesn't require any commands to upload. For the jump server it should look something like this:

```
Host test
     HostName <HOST OF SERVER>
     User <USER>
     Port 22
```

### Steps

1. Download the folder from wherever its stored and place on your local file system.

2. Run `docker run -ti --rm -v <PATH TO ARTIFACT>:import -v ~/.ssh:/root/keys registry.gitlab.com/gileshinchcliff/sheepdipper:latest  -f <PATH TO ARTIFACT> --upload`

3. Follow the prompts in the script to get the file uploaded.

4. Access the files in the location output on the console.
