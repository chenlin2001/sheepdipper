#!/usr/bin/env bats

load /import.sh

setup() {
  cp -R /tests/examples/** /tests/workspace
}

teardown() {
  rm -rf /tests/workspace/*
}

@test "check the help is working" {
  get_help | grep "SheepDipper"
}

@test "check output is being correctly parsed" {
  check_output | grep "COMPLETED"
}

@test "check clamav is updating correctly" {
  freshclam | grep "bytecode.cvd is up to date"
}

@test "check clamav is failing correctly" {
  clamscan /tests/examples/eicar.com | grep "Infected files: 1"
}

@test "check sha3 is working correctly" {
  sha3=$(sha3sum /tests/examples/test-artifacts/test-artifacts/testing.tar.gz)
  check_sha3 `cat /import/SHA3.txt` ${sha3}
}

@test "check you can zip correctly" {
  zip /tests/examples/test-artifacts/ /tests/workspace/test.tar.gz
  ls /tests/workspace | grep test.tar.gz
}

@test "check you can unzip correctly" {
  unzip /tests/examples/test-artifacts/test-artifacts/testing.tar.gz /tests/workspace/
  cat /tests/workspace/import/4 | grep testing
}

@test "encrypting and decrypting files correctly" {
  crypt /tests/examples/test-artifacts/test-artifacts/testing.tar.gz /tests/workspace/test.tar.gz.gpg
  ls /tests/workspace/ | grep test.tar.gz.gpg
  dpass=${pass}
  decrypt /tests/workspace/test /tests/workspace/test.tar.gz
  ls /tests/workspace/ | grep test.tar.gz
}

@test "getting the file list correctly" {
  get_file_list /tests/examples/test-artifacts/test-artifacts/ /tests/workspace/filelist.txt
  compare_files /tests/workspace/filelist.txt /tests/examples/test-artifacts/test-artifacts/
}
