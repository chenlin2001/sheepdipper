#!/usr/bin/env bash

function get_help() {
cat <<EOF
  NAME:
  SheepDipper - A script to automate the process needed to move files from the internet to the secure environment

  USAGE:
  ./sheepdipper.sh [global options] command

  GLOBAL OPTIONS
  -d                   turn on debug
  -h | --help          print the help for this script

  COMMANDS
  --compile            Takes folder of artifacts and processes them and outputs a file ready to be sent to DevOps.
  --upload             Takes processed folder uploads to jump server following all security guidelines.
  --filepath | -f      Required option, points to the folder which hold the files you would either like to import of compile.
EOF
}

function run_tests() {
  bats /tests/
}

function check_output() {
  if [ $? -eq 0 ]
  then
    echo -e "\n\e[92mSUCCESS: COMPLETED ${1} \e[39m\n"
  else
    echo -e "\n\e[91mFAILURE: FAILED ${1} \e[39m\n"
    get_help
    exit 1
  fi
}

function check_virus() {
  freshclam --quiet
  clamscan -r ${1} | grep -q 'Infected files: 0'
  check_output "VIRUS SCAN"
}

function check_sha3() {
  sha3_to_test="${1}"
  sha3_from_file=$(sha3sum "${2}" | cut -d " " -f1)
  ls -la /root/tmp
  echo ${sha3_from_file}
  sha3_results="Input: ${sha3_to_test}\nFile:  ${sha3_from_file}"
  if [[ ${sha3_to_test} == ${sha3_from_file} ]]
  then
    check_output "SHA3 CHECK\n${sha3_results}"
  else
    check_output "SHA3 CHECK\n${sha3_results}"
  fi
}

function zip() {
  tar -zcf ${2} ${1} &> /dev/null
  check_output "COMPRESSING FILES"
}

function unzip() {
  tar -xf ${1} -C ${2} &> /dev/null
  check_output "EXTRACTING FILES"
}

function crypt() {
   pass=$(openssl rand -base64 16)
   echo "${pass}" | gpg -c --passphrase-fd 0 --cipher-algo AES256 --no-verbose --quiet --batch --no-tty -o ${2} --yes ${1}  &> /dev/null
   check_output "ENCRYPTING FILES"
   echo -e "IMPORTANT YOUR PASSWORD IS \e[91m${pass}\e[39m KEEP IT SAFE"
   rm ${1}
}

function decrypt() {
  if [ -z ${dpass} ]; then
    echo -e "\n"
    stty -echo
    read -p "PLEASE ENTER THE PASSWORD TO DECRYPT THE FILE: " dpass; echo
    stty echo
  else
    echo -e "INFO: USING PASSWORD ALREADY SET"
  fi
  echo "${dpass}" | gpg --passphrase-fd 0 --decrypt --output ${2} --batch --no-tty --yes ${1}.tar.gz.gpg  #&> /dev/null
  check_output "DECRYPTING"
}

function get_file_list() {
  find ${1} -type f -print | sed 's|.*/||' | sort > ${2}
  check_output "LISTING FILES"
}

function compile() {
  mkdir ~/tmp/${folder}-${timestamp}
  check_virus /import
  get_file_list /import ~/tmp/${folder}-${timestamp}/filelist.txt
  zip /import/ ~/tmp/${folder}-${timestamp}/${folder}-${timestamp}.tar.gz
  sha3sum ~/tmp/${folder}-${timestamp}/${folder}-${timestamp}.tar.gz | cut -d " " -f1 > ~/tmp/${folder}-${timestamp}/SHA3.txt
  check_output "CREATING SHA3"
  crypt ~/tmp/${folder}-${timestamp}/${folder}-${timestamp}.tar.gz ~/tmp/${folder}-${timestamp}/${folder}-${timestamp}.tar.gz.gpg
  mv ~/tmp/${folder}-${timestamp} /import
}

function compare_files() {
  filelist=$(cat ${1} | sed 's|.*/||' | sort)
  file=$(find ${2} -type f -print | sed 's|.*/||' | sort)
  if [ "${file}"=="${filelist}" ];
  then
    check_output "COMPARING FILES TO FILELIST"
  else
    check_output "COMPARING FILES TO FILELIST"
  fi
}

function manual_check() {
  file_arr=( ${file} )
  filelist_arr=( ${filelist} )
  length=$(echo ${file} ${filelist} | tr ' ' '\n' | wc -L)
  printf "%2s FILE %$((length - 7))s | FILELIST\n"
  printf '%0.s-' $(seq 1 $((length * 2)))
  printf "\n"
  for ((i=0;i<${#file_arr[@]};++i)); do
    filelength=$(echo ${file_arr[i]} | wc -L)
    padding=$((${length} - ${filelength}))
    printf "${file_arr[$i]} %${padding}s |  ${filelist_arr[$i]} \n"
  done
  printf '%0.s-' $(seq 1 $((length * 2)))
  printf "\n\n"
  read -p "PLEASE MANUALLY CONFIRM THE FILELIST MATCHES THE FILES (y/n): " confirm;
  if [[ "${confirm}" =~ ^(y|Y)$ ]]; then
    check_output "MANUAL CONFIRMATION"
  else
    check_output "MANUAL CONFIRMATION"
  fi
}

function copy() {
  cp -r /root/keys/* /root/.ssh/
  chown root:root /root/.ssh
  echo -e "INFO: WE ASSUME YOU HAVE CONFIGURED THIS HOST IN YOUR  SSH CONFIG\n"
  read -p "PLEASE ENTER THE NAME OF THE SERVER YOU'D LIKE TO UPLOAD TO: " host;
  stty -echo
  read -p "PLEASE ENTER THE PASSWORD OF THE SERVER YOU'D LIKE TO UPLOAD TO: " pass;
  stty echo
  sshpass -p ${pass} ssh ${host} "mkdir -p ~/uploads/${fileobject} && mkdir -p ~/tmp/"
  sshpass -p ${pass} scp -rp /import/* ${host}:~/uploads/${fileobject}/
  check_output "MOVING FILES TO REMOTE SERVER"
}

function ssh_commands() {
  sshpass -p ${pass} ssh -q ${host} "/usr/bin/env bash -s ${debug} ${pass} ${dpass} ${fileobject}" <<EOF
  set -x
  if [ "${debug}" == "true" ]; then set -x; fi
  $(typeset -f);
  pass="${pass}"
  dpass=${dpass}
  decrypt ~/uploads/${fileobject}/${fileobject} ~/tmp/${fileobject}.tar.gz
  unzip ~/tmp/${fileobject}.tar.gz ~/tmp/
  #this is a bug which means that when set -x isn't set it will not recognise the password being passed in at sudo -s.
  set -x
  echo "${pass}" | sudo -S freshclam --quiet
  echo "${pass}" | sudo -S clamscan -r ~/tmp/import | grep -q 'Infected files: 0'
  set +x
  check_output "VIRUS SCAN"
  mv ~/tmp/import/ uploads/${fileobject}/
  rm ~/tmp/$fileobject* ~/uploads/${fileobject}/${fileobject}.tar.gz.gpg
  exit 0
EOF
}

function upload() {
  decrypt /import/${fileobject} ~/tmp/${fileobject}.tar.gz
  ls -la /import
  check_sha3 `cat /import/SHA3.txt` ~/tmp/${fileobject}.tar.gz
  unzip ~/tmp/${fileobject}.tar.gz ~/tmp/
  check_virus ~/tmp/import/
  compare_files /import/filelist.txt ~/tmp/import
  manual_check
  copy
  ssh_commands
  check_output "UPLOADING FILES"
  echo -e "\n FIND YOUR FILES HERE ON THE REMOTE SERVER: \e[91m~/uploads/${fileobject}/\e[39m"
}
if [ "$BASH_SOURCE" == "$0" ]; then
  if [[ "${@}" == *"compile"* ]]; then
    echo -e "\nBEGINNING TO COMPILE YOUR FILES";
  elif [[ "${@}" == *"upload"* ]]; then
    echo -e "\nBEGINNING TO UPLOAD YOUR FILES";
  elif [[ "${@}" == *"test"* ]]; then
    echo -e "\nBEGINNING TO RUN TESTS"
  else
    echo -e "\e[91mWARNING: PLEASE SET EITHER COMPILE OR UPLOAD\e[39m\n"
    get_help
    exit 1
  fi

  if [[ $# -gt 0 ]]; then
    while [[ $# -ne 0 ]]
    do
      key="${1}"
      case ${key} in
        --compile | -c)
          cmd=compile
          shift
        ;;
        --upload | -u)
          cmd=upload
          shift
        ;;
        --debug | -d )
          debug=true
          set -x
          shift
        ;;
        --filepath | -f )
          filepath="/import/${2}"
          shift 2
        ;;
        --test )
        run_tests
        exit ${?}
        shift
        ;;
        -h | --help | *)
          get_help
          exit 0
        ;;
      esac
    done
  else
    get_help
  fi
  if [[ -z ${filepath} ]]; then
    echo -e "\e[91mWARNING: YOU MUST PASS IN THE FILEPATH\e[39m\n"
    get_help
    exit 1
  fi
  if [ -z "$(ls -A /import)" ]; then
    echo -e "\e[91mWARNING: NO FILES IN THE FILEPATH\e[39m\n"
    get_help
    exit 1
  fi
  timestamp="$(date '+%Y-%m-%d_%H-%M-%S')"
  fileobject=$(ls /import | grep tar.gz | cut -d'.' -f1)
  folder=$(basename "${filepath}")
  echo ${fileobject}
  ${cmd}
fi
